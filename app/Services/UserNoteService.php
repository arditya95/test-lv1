<?php

namespace App\Services;

use App\Models\User;
use App\Models\Note;

class UserNoteService
{
    public function storeUserNote(User $user, $data)
    {
        $note = $user->userNotes()->create($data);
        return $note;
    }

    public function updateUserNote(int $id, $data)
    {
        $note = Note::findOrFail($id);
        $note->note = $data['note'];
        $note->save();
        return $note;
    }

    public function deleteUserNote(int $id)
    {
        $note = Note::find($id)->delete();
        return $note;
    }
}
