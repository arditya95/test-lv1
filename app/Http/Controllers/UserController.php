<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\CreateUserNoteRequest;
use App\Services\UserNoteService;

class UserController extends Controller
{
    protected $userNoteService;

    public function __construct(UserNoteService $userNoteService)
    {
        $this->userNoteService = $userNoteService;
    }

    public function index()
    {
//        $users = User::get();
//        $users = User::with('userNotes')->get();
        $users = User::with('userNotes')->has('userNotes')->get();
        return view('user', compact('users'));
    }

    public function storeNote(CreateUserNoteRequest $request)
    {
        $note     = $request->validated();
//        (new UserNoteService)->storeUserNote(auth()->user(), $note);
        $this->userNoteService->storeUserNote(auth()->user(), $note);

        return back();
    }

    public function updateNote(CreateUserNoteRequest $request, $noteId)
    {
        $note     = $request->validated();
        $this->userNoteService->updateUserNote($noteId, $note);
        return back();
    }

    public function deleteNote($noteId)
    {
        $this->userNoteService->deleteUserNote($noteId);
        return back();
    }
}
