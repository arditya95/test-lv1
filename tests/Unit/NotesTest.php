<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Models\User;
use App\Models\Note;
use App\Services\UserNoteService;

class NotesTest extends TestCase
{
    public function test_validation_no_id_found_for_delete_note()
    {
        $notes = (new UserNoteService())->deleteUserNote(1);
        $this->assertEquals('no id found', $notes);
    }
}
