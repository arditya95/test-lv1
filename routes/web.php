<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth', 'web'], 'prefix' => ''], function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    //User
    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserController@index')->name('user.index');
        Route::post('/store/note', 'UserController@storeNote')->name('user.store.note');
        Route::put('/update/note/{id}', 'UserController@updateNote')->name('user.update.note');
        Route::delete('/delete/note/{id}', 'UserController@deleteNote')->name('user.delete.note');
    });
});

require __DIR__.'/auth.php';
